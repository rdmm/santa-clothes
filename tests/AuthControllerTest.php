<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;
use Symfony\Component\Process\Process;

class AuthControllerTest extends WebTestCase
{
    public function setUp() {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();
    }

    public function testRegister()
    {
        // Testing route to /register and creating an account
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $this->assertResponseIsSuccessful();

        $this->assertCount(1, $crawler->filter('#myTabContent'));
        $this->assertCount(1, $crawler->filter('#home'));

        $repo = static::$container->get('App\Repository\UserRepository');
        $this->assertCount(2, $repo->findAll());

        $form = $crawler->selectButton("s'inscrire")->form();
        // we fill the fields with testing values 
        $form['user[firstName]'] = 'Nada';
        $form['user[lastName]'] = 'Tnk';
        $form['user[email]'] = 'nada@mail.com';
        $form['user[password][first]'] = 'testtest';
        $form['user[password][second]'] = 'testtest';

        //then we submit the form
        $client->submit($form);
        //We check if we're directed to the index page after the registration
        $this->assertResponseRedirects('/');
        $crawler = $client->request('GET', '/');
        // echo $client->getResponse()->getContent(); 
        $this->assertCount(3, $repo->findAll());

    }
    // Testing route to /login

    public function testLoginRoute(){

        $client = static::createClient();

        $crawler = $client->request('GET', '/login');
        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton("Se connecter")->form();
        // we fill the fields with testing values 
        $form['_username'] = 'mail@mail.com';
        $form['_password'] = 'pppppppp';

        //then we submit the form
        $client->submit($form);
        //We check if we're directed to the index page after the login
        // $this->assertResponseRedirects('/'); //TODO : à faire fonctionner

        $crawler = $client->request('GET', '/');
        echo $client->getResponse()->getContent();


        foreach($crawler->filter('.badge') as $span) {
            $this->assertContains('0', $span->textContent);
        }


        // Testing route to /user/account
        $crawler = $client->request('GET', '/user/account');
        $this->assertResponseIsSuccessful();
        $this->assertCount(1, $crawler->filter('#content'));

        foreach($crawler->filter('#content') as $paragraph) {
            $this->assertContains('Welcome to your profile', $paragraph->textContent);
        }
        // Testing route /user/account/change-password 
        $crawler = $client->request('GET', '/user/account/change-password');
        $this->assertResponseIsSuccessful();
        // we want to modify nada's password so we 
        // select the form submit button 
        $form = $crawler->selectButton("submit")->form();
        // we fill the fields with the new password 
        $form['change_password[password][first]'] = 'nadatest';
        $form['change_password[password][second]'] = 'nadatest';

        //then we submit the form
        $client->submit($form);

        $crawler = $client->request('GET', '/');
        echo $client->getResponse()->getContent();


    }



}
