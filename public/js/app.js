//TODO : trigger function change quantity on value select

// Update quantity and price on shoppingCart
let quantitys = document.querySelectorAll('.lineItem-quantity');

for (let el of quantitys) {

    el.addEventListener('click', (event) => {
        event.preventDefault();

        let itemId = el.getAttribute('data-item');
        let quantity = el.value;
        let path = el.getAttribute('data-path');

        $.ajax({
            url: path,
            // url: '/user/shopping-cart/qty/' + itemId,
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                quantity: quantity
            }),
            success: function (data) {
                if(data.status) {

                    let price = document.querySelector('.total'+itemId);
                    price.textContent = data.total;

                    let totalPriceTwig = document.querySelector('.total-price');
                    totalPriceTwig.textContent = data.totalPrice;
                }
            }
        });
    });
}

function updateProduct() {

}
// trigger update product method
let productsBtn = document.querySelectorAll('.btn-update-product');

for (let el of productsBtn) {
    el.addEventListener('click', (event) => {
        event.preventDefault();
        let elId = el.getAttribute('data-id');
        let name = document.querySelector('#el-product-name' + elId);
        let type = document.querySelector('#el-product-type' + elId);
        let stock = document.querySelector('#el-product-stock' + elId);
        let price = document.querySelector('#el-product-price' + elId);

        if (el.textContent == 'Modifier') {

            let inputName = document.createElement('input');
            inputName.classList.add('col-6');
            inputName.value = name.firstElementChild.textContent;
            name.replaceChild(inputName, name.firstElementChild);

            let inputType = document.createElement('input');
            inputType.classList.add('col-6');
            inputType.value = type.firstElementChild.textContent;
            type.replaceChild(inputType, type.firstElementChild);

            let inputStock = document.createElement('input');
            inputStock.classList.add('col-4');
            inputStock.value = stock.firstElementChild.textContent;
            stock.replaceChild(inputStock, stock.firstElementChild);


            let inputPrice = document.createElement('input');
            inputPrice.classList.add('col-4');
            inputPrice.value = price.firstElementChild.textContent;
            price.replaceChild(inputPrice, price.firstElementChild);

            el.textContent = 'Valider';
            el.classList.remove('btn-primary');
            el.classList.add('btn-warning');

        } else if (el.textContent == 'Valider') {
            el.textContent = 'Modifier';
            el.classList.remove('btn-warning');
            el.classList.add('btn-primary');

            let inputName = name.firstElementChild;
            let newNameValue = inputName.value;

            let nameSpan = document.createElement('span');
            nameSpan.textContent = newNameValue;

            let inputType = type.firstElementChild;
            let newTypeValue = inputType.value;

            let typeSpan = document.createElement('span');
            typeSpan.textContent = newTypeValue;

            let inputStock = stock.firstElementChild;
            let newStockValue = inputStock.value;

            let stockSpan = document.createElement('span');
            stockSpan.textContent = newStockValue;

            let inputPrice = price.firstElementChild;
            let newPriceValue = inputPrice.value;

            let priceSpan = document.createElement('span');
            priceSpan.textContent = newPriceValue;

            $.ajax({
                url: '/admin/products/update/' + elId,
                type: "POST",
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    name: newNameValue,
                    type: newTypeValue,
                    stock: newStockValue,
                    price: newPriceValue
                }),
                success: function (data) {
                    if(data.status) {

                        name.replaceChild(nameSpan, name.firstElementChild);
                        type.replaceChild(typeSpan, type.firstElementChild);
                        stock.replaceChild(stockSpan, stock.firstElementChild);
                        price.replaceChild(priceSpan, price.firstElementChild);
                    }
                }
            });

        }

    });
}

//modal confirm add shopping cart - try top merge new diff into master
let addToCartBtn = document.querySelector('#btn-add-to-cart');

if(addToCartBtn) {
    addToCartBtn.addEventListener('click', (event) => {
        event.preventDefault();
        let path = addToCartBtn.getAttribute('data-path');
        // let id = addToCartBtn.getAttribute('data-id');
        $.ajax({
            url: path,
            // url: "{{ path('add_product_to_cart', {'id': id})}}",
            // url: '/user/addproduct/' + id,
            type: 'GET',
            success: function (data) {
                if(data.status) {
                    let cart = document.querySelector('.shopping-cart-number');
                    let cartNb = cart.textContent;
                    cart.textContent = parseInt(cartNb) + 1;

                    let modal = document.querySelector('#modalAddProduct');
                    modal.classList.add('show', 'd-block', 'dark-backdrop');
                    modal.setAttribute('aria-modal', true);
                    modal.removeAttribute('aria-hidden');

                    let closeBtns = document.querySelectorAll('.close-modal');
                    for (btn of closeBtns) {
                        btn.addEventListener('click', (event) => {
                            modal.classList.remove('show', 'd-block', 'dark-backdrop');
                            modal.removeAttribute('aria-modal');
                            modal.setAttribute('aria-hidden', true);
                        });
                    }
                }
            }
        });
    });
}

function selectStockByGender () {
    let genderSelect = document.querySelector('#gender');
    $.ajax({

        url: '/' + genderSelect.value,
        type: 'GET',
        success: function (data) {
            document.body.innerHTML = data.response;

        }
    });
}


function selectStockByOptions () {
    let genderSelect = document.querySelector('#gender');
    let catSelect = document.querySelector('#category');
    $.ajax({
        url: '/' + genderSelect.value + '/' + catSelect.value,
        type: 'GET',
        success: function (data) {

            document.body.innerHTML = data.response;
        }
    });
}



