<h1> Projet E-commerce</h1>
RDMM jeune Start-UP composée de 4 jeunes Simplonniens a pour mission de créer un site e-commerce de vêtements "Santa-Clothes " pour son client "  ", le projet sera utilisé avec les techniques suivantes :

### Technique

- Utilisation Symfony 4.3 avec la stack complète (Doctrine, sécurité, twig, etc.)
- Application responsive (bootstrap/bootswatch/etc.)
- Utilisation de git/gitlab pour le travail en groupe

Pour concevoir le site nous nous sommes basés sur les Users storie ainsi que le Diagram de class suivant:

<h2>USER STORIES</h2>

* En tant qu'utilisateur je veux pouvoir accéder à la fiche d'un produit pour faire mon choix sur les différentes options.

* En tant qu'utilisateur je veux pouvoir avoir un classement par categorie pour faciliter ma recherche.

* En tant qu'utilisateur je veux pouvoir connaître la disponibilité du produit pour pouvoir l'acheter directement.

* En tant qu'utilisateur non connecté, je veux pouvoir créer un compte / me connecter pour accéder à mon profile et visualiser mon historique de commandes.

* En tant qu'utilisateur connecté, je veux pouvoir commenter et noter un produit pour aider les autres utilisateurs à faire leur choix.

* En tant qu'administrateur, je veux pouvoir gérer le calogue de produits pour rajouter des nouveaux produits en stock.

* En tant qu'utilisateur connecté je veux pouvoir acceder à mon panier pour voir les produits que j'y ai ajouté

<h3>USE CASES</h3>
![santay-clothes-use-case-diagram](./conception/Santa_Clothes_Use_Case_Diagram.png)



<h4>MAQUETTE</h4>
<p>Mobile</p>
![santa-clothes-maquette](./conception/Maquette Mobile.png)



#### PC



![santa-clothes-maquette](./conception/Maquette PC.png)

## Utilisation du projet

### Composer

Dans un premier temps, il faut lancer la commande `$ composer install`  pour installer toutes les extensions de symfony. On pourra retrouver l'ensemble de leurs fichiers dans le dossier `vendor`.

### Base de donnée

Ensuite, il faudra créer un fichier `.env.local` à la racine du projet, dans lequel il faudra spécifier vos données de connexion en ajoutant la ligne suivante en nommant la base de donnée `santa_clothes` : 

`DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name`

