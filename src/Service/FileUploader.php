<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;

class FileUploader {


    public function upload(File $file): string {
        $name = 'uploads/' . uniqid() . '.' . $file->guessExtension();

        $file->move(__DIR__ .'/../../public/uploads', $name);

        return $name;

    }
}
