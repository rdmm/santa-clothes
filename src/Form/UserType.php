<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',TextType::class,[
                'attr' => 
                [
                    'placeholder' =>" First Name *"
                ]
            ])
            ->add('lastName',TextType::class,[
                'attr' => 
                [
                    'placeholder' =>" Last name *"
                ]
            ])
            ->add('email' ,EmailType::class,[
                'attr' => 
                [
                    'placeholder' =>" Email * "
                ]
            ])
            // ->add('roles')
            ->add('password', RepeatedType::class, [
                
                'type' => PasswordType::class

            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btnRegister'],
            ]);
            // ->add('address', TextType::class,[
            //     'attr' => 
            //     [
            //         'placeholder' =>" address * "
            //     ]
            // ])
            
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => true,
        ]);
    }
}
