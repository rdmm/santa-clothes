<?php

namespace App\Form;

use App\Entity\Options;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('type')
//            ->add('options', CollectionType::class, [
//                'entry_type' => CategoryType::class,
//                'entry_options' => ['label' => true]
//            ])
            ->add('options', EntityType::class, [
                'class' => Options::class,
                'choice_label' => 'label',
                'multiple' => true,
                'required' => false
            ])
            ->add('price')
            ->add('uploadImage', FileType::class, [
                'required' => false,
                'label' => false,
                'attr' => ['height'=> '5px', 'width' => '5px']
            ])
//            ->add('imageSize')
            ->add('stock')
//            ->add('options')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
