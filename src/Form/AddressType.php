<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street', TextType::class,[
                'attr' => 
                [
                    'placeholder' =>"Nom de la Rue"
                ]
            ])
            ->add('streetNumber',NumberType::class,[
                'attr' => 
                [
                    'placeholder' =>" Num de la rue "
                ]
            ])
            ->add('zipCode',TextType::class)
            ->add('city',TextType::class)
            ->add('country', ChoiceType::class, [
                'choices' => [
                    'France' => 'france',
                    'USA' => 'USA',
                    'Tunisia'   => 'Tunisia',
                    'Algeria' => 'Algeria',
                    
                ],
            ])
            ->add('phone',TextType::class)
            ;

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
