<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
   private $encoder;
   public function __construct(UserPasswordEncoderInterface $encoder)
   {
       $this->encoder = $encoder;
   }
    public function load(ObjectManager $manager)
    {
     
         $user = new User();
         $password = $this->encoder->encodePassword($user , 'pppppppp');
         $user->setEmail('mail@mail.com')
            ->setPassword($password)
            ->setfirstName('test')
            ->setLastName('test')
            ->setRoles(['ROLE_USER']);
         $manager->persist($user);
    
         $user = new User();
         $password = $this->encoder->encodePassword($user , 'adminadmin');
         $user->setEmail('admin@admin.com')
            ->setPassword($password)
            ->setfirstName('admin')
            ->setLastName('admin')
            ->setRoles(['ROLE_ADMIN']);

         $manager->persist($user);

    $manager->flush();

   }
}