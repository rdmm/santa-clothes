<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Color;
use App\Entity\Gender;
use App\Entity\Image;
use App\Entity\Product;
use App\Entity\Size;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Form\Type\VichImageType;

class OptionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $genders = ['Hommes', 'Femmes', 'Enfants'];
        $cats = ['Haut', 'Bas', 'Chaussures', 'Accessoires'];
        $colors = ['Bleu', 'Gris', 'Vert', 'Rouge Brique', 'Anthracite'];
        $sizes = ['XS', 'S', 'M', 'L', 'XL', 'XXL'];

        for ($i = 0; $i < count($genders); $i++) {
            $gender = new Gender();
            $gender->setLabel($genders[$i]);
            $manager->persist($gender);
            $gendersArray[] = $gender;
        }
        for ($j=0; $j < count($cats); $j++) {
            $cat = new Category();
            $cat->setLabel($cats[$j]);
            $manager->persist($cat);
            $catsArray[] = $cat;
        }
        for ($l=0; $l < count($colors); $l++) {
            $color = new Color();
            $color->setLabel($colors[$l]);
            $manager->persist($color);
            $colorsArray[] = $color;
        }
        for ($m=0; $m < count($sizes); $m++) {
            $size = new Size();
            $size->setLabel($sizes[$m]);
            $manager->persist($size);
            $sizeArray[] = $size;
        }

//        foreach ($gendersArray as $gender) {
//            foreach ($catsArray as $cat) {
//                foreach ($sizeArray as $size) {
//                    foreach ($colorsArray as $color) {
//                        $product = new Product();
//                        $type = 'pantsM';
//                        $imgName = $type.'.jpg';
//                        $src = './public/tmp/'.$imgName;
//                        $uploadImage = new File($src);
//                        $image = new Image();
//                        $image->setImageName($imgName);
//                        $image->setImageFile($uploadImage);
//                        $manager->persist($image);
//                        $product
//                            ->setName('Test Homme boucles imbriquées')
//                            ->setType('pants')
//                            ->setPrice(49.99)
//                            ->setImage($image)
//                            ->setStock(40)
//                            ->addOption($gender)
//                            ->addOption($cat)
//                            ->addOption($size)
//                            ->addOption($color);
//                        // ->addOption($bash);
//                        $manager->persist($product);
//                    }
//                }
//            }
//        }

        for ($k = 0; $k < 10; $k++) {
            $product1 = new Product();
            $type1 = 'pantsM';
            $imgName1 = 'uploads/'.$type1.'.jpg';
            $product1
                ->setName('Test ' . $k . 'Homme')
                ->setType('pants')
                ->setPrice(49.99)
                ->setImage($imgName1)
                ->setStock(40)
                ->addOption($gendersArray[0])
                ->addOption($catsArray[1]);
            // ->addOption($bash);
            $manager->persist($product1);

            $product4 = new Product();
            $type4 = 'pullM';
            $imgName4 = 'uploads/'.$type4.'.jpg';
            $product4
                ->setName('Test ' . $i . ' Homme')
                ->setType('pull')
                ->setPrice(12.99)
                ->setImage($imgName4)
                ->setStock(40)
                ->addOption($gendersArray[0])
                ->addOption($catsArray[0]);
            // ->addOption($hauth);
            $manager->persist($product4);

            $product6 = new Product();
            $type6 = 'pullW';
            $imgName6 = 'uploads/'.$type6.'.jpg';
            $product6
                ->setName('Test ' . $i . ' Femme')
                ->setType('pull')
                ->setPrice(8.99)
                ->setImage($imgName6)
                ->setStock(10)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[0]);
            // ->addOption($hautf);
            $manager->persist($product6);

            $product3 = new Product();
            $type3 = 'pantsK';
            $imgName3 = 'uploads/'.$type3.'.jpg';
            $product3
                ->setName('Test ' . $i . ' Enfants')
                ->setType('pants')
                ->setPrice(19.99)
                ->setImage($imgName3)
                ->setStock(13)
                ->addOption($gendersArray[2])
                ->addOption($catsArray[1]);
            // ->addOption($hautE);
            $manager->persist($product3);


            $product5 = new Product();
            $type5 = 'pullK';
            $imgName5 = 'uploads/'.$type5.'.jpg';
            $product5
                ->setName('Test ' . $i . ' Enfants')
                ->setType('pull')
                ->setPrice(12.99)
                ->setImage($imgName5)
                ->setStock(100)
                ->addOption($gendersArray[2])
                ->addOption($catsArray[0]);
            // ->addOption($basE);
            $manager->persist($product5);

            $product7 = new Product();
            $type7 = 'sport-pantsM';
            $imgName7 = 'uploads/'.$type7.'.jpg';
            $product7
                ->setName('Sweat Pants ' . $i . 'Homme')
                ->setType('pants')
                ->setPrice(42.99)
                ->setImage($imgName7)
                ->setStock(20)
                ->addOption($gendersArray[0])
                ->addOption($catsArray[1]);
            $manager->persist($product7);

            $product8 = new Product();
            $type8 = 'BootsW';
            $imgName8 = 'uploads/'.$type8.'.jpg';
            $product8
                ->setName('Boots  ' . $i . 'Femme')
                ->setType('shoes')
                ->setPrice(50.00)
                ->setImage($imgName8)
                ->setStock(4)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[2]);
            $manager->persist($product8);

            $product9 = new Product();
            $type9 = 'BottinesW';
            $imgName9 = 'uploads/'.$type9.'.jpg';
            $product9
                ->setName('bottines ' . $i . ' Femme')
                ->setType('shoes')
                ->setPrice(59.99)
                ->setImage($imgName9)
                ->setStock(40)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[2]);
            $manager->persist($product9);

            $product10 = new Product();
            $type10 = 'HeelsW';
            $imgName10 = 'uploads/'.$type10.'.jpg';
            $product10
                ->setName('bottines ' . $i . 'Femme')
                ->setType('shoes')
                ->setPrice(45.00)
                ->setImage($imgName10)
                ->setStock(20)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[2]);
            $manager->persist($product10);

            $product11 = new Product();
            $type11 = 'PullGreyW';
            $imgName11 = 'uploads/'.$type11.'.jpg';
            $product11
                ->setName('Pull moulant')
                ->setType('Haut')
                ->setPrice(10.00)
                ->setImage($imgName11)
                ->setStock(44)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[0]);
            $manager->persist($product11);

            $product12 = new Product();
            $type12 = 'pull1W';
            $imgName12 = 'uploads/'.$type12.'.jpg';
            $product12
                ->setName('Pull ' . $i . ' Femme')
                ->setType('Haut')
                ->setPrice(16.66)
                ->setImage($imgName12)
                ->setStock(44)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[0]);
            $manager->persist($product12);

            $product13 = new Product();
            $type13 = 'pullCapucheW';
            $imgName13 = 'uploads/'.$type13.'.jpg';
            $product13
                ->setName('Pull à Capuche ' . $i . ' Femme')
                ->setType('Haut')
                ->setPrice(20.25)
                ->setImage($imgName13)
                ->setStock(5)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[0]);
            $manager->persist($product13);

            // Fixtures homme catégorie haut

            $product14 = new Product();
            $type14 = 'balenciagaM';
            $imgName14 = 'uploads/'.$type14.'.jpg';
            $product14
                ->setName('Pull Noir ' . $i . ' Homme ')
                ->setType('Haut')
                ->setPrice(20.25)
                ->setImage($imgName14)
                ->setStock(55)
                ->addOption($gendersArray[0])
                ->addOption($catsArray[0]);
            $manager->persist($product14);


            $product15 = new Product();
            $type15 = 'coatM';
            $imgName15 = 'uploads/'.$type15.'.jpeg';
            $product15
                ->setName('Coat ' . $i . ' Homme ')
                ->setType('Haut')
                ->setPrice(20.25)
                ->setImage($imgName15)
                ->setStock(55)
                ->addOption($gendersArray[0])
                ->addOption($catsArray[0]);
            $manager->persist($product15);

            $product24 = new Product();
            $type24 = 'shoesM';
            $imgName24 = 'uploads/'.$type24.'.jpg';
            $product24
                ->setName('Chaussures Homme ')
                ->setType('Chaussures')
                ->setPrice(60.00)
                ->setImage($imgName24)
                ->setStock(56)
                ->addOption($gendersArray[0])
                ->addOption($catsArray[2]);
            $manager->persist($product24);

            $product25 = new Product();
            $type25 = 'Hommes_Chaussures';
            $imgName25 = 'uploads/'.$type25.'.jpg';
            $product25
                ->setName('Basket Homme ')
                ->setType('Chaussures')
                ->setPrice(49.99)
                ->setImage($imgName25)
                ->setStock(3)
                ->addOption($gendersArray[0])
                ->addOption($catsArray[2]);
            $manager->persist($product25);
            // Fixtures Femme Catégorie Bas aka pants / shorts

            $product2 = new Product();
            $type2 = 'pantsW';
            $imgName2 = 'uploads/'.$type2.'.jpg';
            $product2
                ->setName('Pantalon Rose')
                ->setType('pantalon')
                ->setPrice(15.99)
                ->setImage($imgName2)
                ->setStock(100)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[1]);
            // ->addOption($basf);
            $manager->persist($product2);

            $product16 = new Product();
            $type16 = 'pants1W';
            $imgName16 = 'uploads/'.$type16.'.jpeg';
            $product16
                ->setName(' High waisted Jeans')
                ->setType('pantalon')
                ->setPrice(15.99)
                ->setImage($imgName16)
                ->setStock(99)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[1]);
            // ->addOption($basf);
            $manager->persist($product16);

            $product17 = new Product();
            $type17 = 'pants2W';
            $imgName17 = 'uploads/'.$type17.'.jpeg';
            $product17
                ->setName( 'Striped Pants')
                ->setType('pantalon')
                ->setPrice(15.99)
                ->setImage($imgName17)
                ->setStock(99)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[1]);
            // ->addOption($basf);
            $manager->persist($product17);


            $product18 = new Product();
            $type18 = 'pants3W';
            $imgName18 = 'uploads/'.$type18.'.jpeg';
            $product18
                ->setName(' Pantalon Femme Gris ')
                ->setType('pantalon')
                ->setPrice(15.99)
                ->setImage($imgName18)
                ->setStock(99)
                ->addOption($gendersArray[1])
                ->addOption($catsArray[1]);
            // ->addOption($basf);
            $manager->persist($product18);


             // ACCESSOIRES

             $product19 = new Product();
             $type19 = 'passport';
             $imgName19 = 'uploads/'.$type19.'.jpg';
             $product19
                 ->setName(' Passport Holder ')
                 ->setType('Accessoires')
                 ->setPrice(13.99)
                 ->setImage($imgName19)
                 ->setStock(86)
                 ->addOption($gendersArray[0])
                 ->addOption($gendersArray[1])
                 ->addOption($catsArray[3]);
             // ->addOption($basf);
             $manager->persist($product19);

             $product20 = new Product();
             $type20 = 'powerBank';
             $imgName20 = 'uploads/'.$type20.'.jpg';
             $product20
                 ->setName('Power Bank ')
                 ->setType('Accessoires')
                 ->setPrice(20.50)
                 ->setImage($imgName20)
                 ->setStock(100)
                 ->addOption($gendersArray[0])
                 ->addOption($gendersArray[1])
                 ->addOption($catsArray[3]);
             // ->addOption($basf);
             $manager->persist($product20);

             $product21 = new Product();
             $type21 = 'accessoire2';
             $imgName21 = 'uploads/'.$type21.'.jpg';
             $product21
                 ->setName('Black Hat ')
                 ->setType('Accessoires')
                 ->setPrice(15.50)
                 ->setImage($imgName21)
                 ->setStock(3)
                 ->addOption($gendersArray[0])
                 ->addOption($gendersArray[1])
                 ->addOption($catsArray[3]);
             // ->addOption($basf);
             $manager->persist($product21);


             $product22 = new Product();
             $type22 = 'Accessoire1';
             $imgName22 = 'uploads/'.$type22.'.jpg';
             $product22
                 ->setName('Phone Case')
                 ->setType('Accessoires')
                 ->setPrice(15.50)
                 ->setImage($imgName22)
                 ->setStock(33)
                 ->addOption($gendersArray[0])
                 ->addOption($gendersArray[1])
                 ->addOption($catsArray[3]);
             // ->addOption($basf);
             $manager->persist($product22);

             // Fixtures for kids 

             $product23 = new Product();
             $type23 = 'Enfants_Chaussures';
             $imgName23 = 'uploads/'.$type23.'.jpg';
             $product23
                 ->setName('Kids shoes')
                 ->setType('Chaussures')
                 ->setPrice(15.50)
                 ->setImage($imgName23)
                 ->setStock(33)
                 ->addOption($gendersArray[2])
                 ->addOption($catsArray[2]);
             // ->addOption($basf);
             $manager->persist($product23);

             $product23 = new Product();
             $type23 = 'Enfants_Accessoires';
             $imgName23 = 'uploads/'.$type23.'.jpg';
             $product23
                 ->setName('girls backpack')
                 ->setType('Accessoires')
                 ->setPrice(15.50)
                 ->setImage($imgName23)
                 ->setStock(33)
                 ->addOption($gendersArray[2])
                 ->addOption($catsArray[3]);
             $manager->persist($product23);

        }

        $manager->flush();
    }
}
