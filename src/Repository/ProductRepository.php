<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Gender;
use App\Entity\Options;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Product[] Returns an array of Product objects
     */
    public function findByType($options, $type)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.type = :type')
            ->setParameter('type', $type)
            ->innerJoin('p.options', 'o')
            ->addSelect('o')
            ->andWhere('o = :options')
            ->setParameter('options', $options)
            ->getQuery()
            ->getResult()
            ;
        dump($qb);

        return $qb;
    }

//    public function findByCat(Category $cat) {
//        $qb = $this->createQueryBuilder('p')
//            ->select('p')
//            ->innerJoin('p.options', 'o', 'WITH', 'o = :cat')
//            ->addSelect('o')
////            ->innerJoin('Gender', 'g')
////                ->andWhere('o = :cat')
//            ->setParameter(':cat' , $cat )
//            ->getQuery()
//            ->getResult();
//        dump($qb);
//
//        return $qb;
//    }



    public function findByOptions(Options $option1, Options $option2)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere(':option1 MEMBER OF p.options')
            ->setParameter(':option1', $option1)
            ->andWhere(':option2 MEMBER OF p.options')
            ->setParameter(':option2', $option2)
            ->getQuery()
            ->getResult();

        return $qb;
    }

    public function findByCat(Options $gender, Options $cat)
    {
        $sub1 = $this->createQueryBuilder('p')
            ->innerjoin('p.options', 'o')
            ->addSelect('o')
            ->andWhere('o = :options')
            ->setParameter(':options', $gender);

        dump($sub1);

        $qb = $this->createQueryBuilder('p');

        $query = $qb
            ->innerjoin('p.options', 'o')
            ->addSelect('o')
            ->andWhere('o = :options')
            ->setParameter(':options', $cat)
            ->where($qb->expr()->in('o', $sub1->getDQL()));
//            ->getQuery()
//            ->getResult();
 //            ->andWhere('o = :options2')
//            ->setParameter('options2', $cat)
        dump($query);

        return $query;
    }

    /** TODO Boucle sur tableau et add as param à la query */

   public function findByKeyword(string $keyword)
   {

   // on met les "%" avant et après le mot clé pour qu'il cherche ce qui contient mm si il y a des choses avant et apres (doit etre fait à l'exterieur de la requette SQL)
       $key = '%'.$keyword.'%';

       $entityManager = $this->getEntityManager();

       // on fait une requette "FROM App\Entity..." et pas directement sur une Table SQL
       // il faut donc mettre un alias (ici "product"), pour qu'il la prenne en compte 
       // il va la considérer comme une "instance de la table" et chercher dedans.

       // là je le fait chercher dans le nom, la categorie et la description, parce que à priori c est les endroits ou un mot clé peut sortir
       $query = $entityManager->createQuery(
           'SELECT product
           FROM App\Entity\Product product
           WHERE product.name LIKE :key 
           OR product.type LIKE :key'
          
       )->setParameter('key', $key);

       // returns an array of Product objects
       return $query->getResult();
   }
}
