<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Address;
use App\Form\ChangePasswordType;
use App\Form\AddressType;
use App\Repository\AddressRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends AbstractController
{
    /**
     * @Route("/user/account", name="account")
     */
    public function index()
    {

        return $this->render('account/account.html.twig');
    }

    /**
     * @Route("/user/address", name="address"))
     * @param Request $request
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    
    public function address(Request $request, ObjectManager $manager)
    {
        $address = new Address();

        $form = $this->createForm(AddressType::class, $address);
        $user = $this->getUser();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // dd('hello');
            // var_dump($address);
            $user->addAddress($address);
            $manager->persist($address);
            // $manager
            $manager->flush();

        }
        return $this->render('account/address.html.twig', array(
            'address' => $form->createView(),
        ));
    }

    /**
     * @Route("/user/account/change-password", name="change_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = $this->getUser();
        dump($user);
        $form = $this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $encodedPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encodedPassword);
            dump($user->getPassword());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'Votre nouveau mot de passe est enregistré !');
            
            return $this->redirectToRoute('logout');
        }

        return $this->render('account/change-password.html.twig', array(
            'form'    => $form->createView(),
        ));
    }
}
