<?php

namespace App\Controller;

use App\Entity\Gender;
use App\Entity\LineItem;
use App\Entity\Options;
use App\Entity\Orders;
use App\Entity\Product;
use App\Entity\ShoppingCart;
use App\Entity\User;
use App\Repository\ColorRepository;
use App\Repository\GenderRepository;
use App\Repository\LineItemRepository;
use App\Repository\OptionsRepository;
use App\Repository\ProductRepository;
use App\Repository\SizeRepository;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LineItemController extends AbstractController
{
    /**
     * @Route("/user/shopping-cart", name="shopping_cart")
     */
    public function index()
    {
        $connectedUserLineItems = $this->getUser()->getLineItems();

        $total = null;

        foreach ($connectedUserLineItems as $lineItem) {
            $total += $lineItem->getPrice();
        }

        return $this->render('shopping_cart/index.html.twig', [
            'total' => $total
        ]);
    }

    /**
     * @Route("/user/addproduct/{id}", name="add_product_to_cart")
     */
    public function addShoppingCart(Product $product, ObjectManager $om)
    {
        $user = $this->getUser();

        $lineItem = new LineItem();

        $lineItem->setProduct($product)
            ->setQuantity(1)
            ->setPrice($product->getPrice())
            ->setUser($user);

        $om->persist($lineItem);

        $currentStock = $product->getStock();

        $product->setStock($currentStock - 1);

        $user->addLineItem($lineItem);

        $om->flush();

        return $this->json([
            'status' => true,
        ]);
    }

    public function getUserLineItems(string $view) {
        $user = $this->getUser();
        $lineItems = $user->getLineItems();

        $total = null;

        foreach ($lineItems as $lineItem) {

            $total += $lineItem->getPrice();
        }

        return $this->render($view, [
            'total' => $total
        ]);
    }

    /**
     * @Route("/user/shopping-cart/checkout", name="checkout_shopping_cart")
     */
    public function checkoutShoppingCart()
    {
        return $this->getUserLineItems('shopping_cart/sidebar.html.twig');
    }

    /**
     * @Route("/user/deleteline/{id}", name="delete_line")
     */
    public function deleteLineItem(LineItem $lineItem, ObjectManager $om)
    {
        $product = $lineItem->getProduct();

        $currentStock = $product->getStock();

        $product->setStock($currentStock + 1);

        $om->remove($lineItem);

        $om->flush();

        return $this->redirectToRoute('shopping_cart', [
            'id' => $lineItem->getId()
        ]);
    }

    //TODO : function update quantity

    /**
     * @Route("/user/shopping-cart/qty/{id}", name="update_quantity")
     */
    public function updateQuantity(Request $request, LineItem $lineItem, ObjectManager $om)
    {
        $decodedQty = json_decode($request->getContent());

        $qty = $decodedQty->quantity;

        $previousQty = $lineItem->getQuantity();
        $currentStock =  $lineItem->getProduct()->getStock();

        if ($currentStock + $previousQty + 1 >= $qty) {

            $lineItem->setQuantity($qty);
            $lineItem->setPrice($lineItem->getProduct()->getPrice() * $qty);

            $lineItem->getProduct()->setStock($currentStock + $previousQty - $qty);
        }

        $om->flush();

        $total = $lineItem->getPrice();

        $lineItems = $this->getUser()->getLineItems();
        $totalPrice = null;

        foreach ($lineItems as $lineItem) {
            $totalPrice += round($lineItem->getPrice(),2);
        }

        return $this->json([
            'status' => true,
            'total' => round($total, 2),
            'totalPrice' => round($totalPrice, 2)
        ], 201);
    }
  


}
