<?php

namespace App\Controller;

use App\Entity\Gender;
use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\Options;
use App\Entity\Product;
use App\Form\CommentType;
use App\Form\ProductType;
use App\Repository\CategoryRepository;
use App\Repository\ColorRepository;
use App\Repository\CommentRepository;
use App\Repository\GenderRepository;
use App\Repository\OptionsRepository;
use App\Repository\ProductRepository;
use App\Repository\SizeRepository;
use App\Service\FileUploader;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;



class ProductController extends AbstractController
{
    //TODO create controller for rendering Gender and Options
    /**
     * @param GenderRepository $genderRepo
     * @param CategoryRepository $catRepo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showOptions(GenderRepository $genderRepo, CategoryRepository $catRepo)
    {
        $genders = $genderRepo->findAll();
        $cats = $catRepo->findAll();

        return $this->render('components/nav.html.twig', [
            'genders' => $genders,
            'cats' => $cats
        ]);
    }
      /**
     * @param GenderRepository $genderRepo
     * @param CategoryRepository $catRepo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showOptionsFooter(GenderRepository $genderRepo, CategoryRepository $catRepo)
    {
        $genders = $genderRepo->findAll();
        $cats = $catRepo->findAll();

        return $this->render('components/footer.html.twig', [
            'genders' => $genders,
            'cats' => $cats
        ]);
    }
    

    /**
     * @Route("/", name="index")
     */
    public function index(GenderRepository $genderRepo, CategoryRepository $catRepo)
    {
//        dd($this->getUser()->getRoles());

        if ($this->getUser() && in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            return $this->redirectToRoute('admin_panel');
        } else {


            $genders = $genderRepo->findAll();
            $cats = $catRepo->findAll();
            return $this->render('product/index.html.twig', [
                'genders' => $genders,
                'cats' => $cats
            ]);
        }
    }

    /**
     * @Route("/{id}", name="product_by_gender")
     */
    public function productByGender(Request $request, Options $options,
                                    GenderRepository $genderRepo, CategoryRepository $catRepo
    )
    {
        $genders = $genderRepo->findAll();
        $cats = $catRepo->findAll();

        if ($request->isXmlHttpRequest()) {

            $response = [
                "code" => 200,
                'response' => $this->render('product/stocks.html.twig', [
                    'options' => $options,
                    'genders' => $genders,
                    'cats' => $cats])->getContent()];

            return $this->json($response);
        }

        return $this->render('product/list.html.twig', [
            'options' => $options,
            'genders' => $genders,
            'cats' => $cats
        ]);
    }

    /**
     * @Route("/product/{id}", name="one_product")
     */
    public function oneProduct(
        Product $product,
        OptionsRepository $repo,
        GenderRepository $genderRepo,
        CategoryRepository $catRepo,
        ColorRepository $colRepo,
        SizeRepository $sizeRepo
    )
    {

        $options = $repo->findAll();
        $genders = $genderRepo->findAll();
        $cats = $catRepo->findAll();
        $colors = $colRepo->findAll();
        $sizes = $sizeRepo->findAll();
        $form = $this->createForm(CommentType::class);

        return $this->render('product/one-product.html.twig', [
            'oneproduct' => $product,
            'options' => $options,
            'genders' => $genders,
            'cats' => $cats,
            'colors' => $colors,
            'sizes' => $sizes,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{gender}/{cat}", name="product_by_options")
     */
    public function productByOptions(Request $request ,Options $gender, Options $cat, ProductRepository $repo, GenderRepository $genderRepo, CategoryRepository $catRepo)
    {
        $products = $repo->findByOptions($gender, $cat);
        $genders = $genderRepo->findAll();
        $cats = $catRepo->findAll();

        if ($request->isXmlHttpRequest()) {

            $response = [
                "code" => 200,
                'response' => $this->render('product/stocks.html.twig', [
                    'products' => $products,
                    'genders' => $genders,
                    'gender' => $gender,
                    'cats' => $cats,
                    'cat' => $cat])->getContent()];

            return $this->json($response);
        }


        return $this->render('product/listCat.html.twig', [
            'products' => $products,
            'genders' => $genders,
            'gender' => $gender,
            'cats' => $cats,
            'cat' => $cat
        ]);
    }
    


    /**
     * @Route("/admin/products/stocks", name="product_stocks")
     */
    public function stocks(ProductRepository $repo, GenderRepository $genderRepository, CategoryRepository $categoryRepository)
    {
        $products = $repo->findBy([], ['id' => 'DESC']);
        $genders = $genderRepository->findBy([]);
        $cats = $categoryRepository->findBy([]);

        return $this->render('product/stocks.html.twig', [
            'products' => $products,
            'genders' => $genders,
            'cats' => $cats
        ]);
    }

    /**
     * @Route("/admin/products/add", name="add_product")
     */
    public function addProduct(Request $request, ObjectManager $manager, FileUploader $uploader)
    {
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $imagePath = $uploader->upload($product->getUploadImage());
            $product->setImage($imagePath);

            $manager->persist($product);
            $manager->flush();

            return $this->redirectToRoute('product_stocks');
        }

        return $this->render('product/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/search/product/results", name="search_results")
     */
    public function search_results(ProductRepository $repo, Request $request)
    {
        $found = false;

        $search = $request->get('Search');


        $products = $repo->findByKeyword($search);

        if ($products == []) {
            $found = true;
        }
        return $this->render('product/listCat.html.twig', [
            'search' => $search,
            'products' => $products,
            'found' => $found

        ]);
    }

    /**
     * @Route("/user/product/{id}/comment/add", name="add_comment")
     */
    public function addComment(Request $request, ObjectManager $manager)
    {
        $comment = new Comment();


        dd($comment);

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setUser($this->getUser())
                ->setProduct($this->getProduct());
            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('one_product');
        }
        return $this->render('product/one-product.html.twig', [
            'form' => $form->createView()
        ]);
    }

  
    //TODO: créer route modif Product

    /**
     * @param Request $request
     * @Route("/admin/products/update/{id}", name="update_product")
     */
    public function updateProduct(Product $product, Request $request, ObjectManager $manager)
    {
        $decodeValues = json_decode($request->getContent());

        $name = $decodeValues->name;
        $type = $decodeValues->type;
        $stock = $decodeValues->stock;
        $price = $decodeValues->price;

        $product->setName($name);
        $product->setType($type);
        $product->setStock($stock);
        $product->setPrice($price);

        $manager->flush();

        return $this->json([
            'status' => true,
        ], 201);
    }

    /**
     * @Route("/admin/products/remove/{id}", name="remove_product")
     */
    public function removeProduct(Product $product, ObjectManager $manager)
    {
        $manager->remove($product);
        $manager->flush();

        return $this->redirectToRoute('product_stocks');
    }
    
    //TODO : search product with keyword
}
