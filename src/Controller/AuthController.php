<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\GenderRepository;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class AuthController extends AbstractController

{
    /**
     * @Route("/login", name="login")
     */
    public function index(AuthenticationUtils $utils,GenderRepository $repo)
    {
        $error = $utils->getLastAuthenticationError();
        $lastusername = $utils->getLastUsername();

        $genders = $repo->findAll();

        return $this->render('auth/login.html.twig',[
            'error' =>  $error,
            'username' => $lastusername,
            'genders' => $genders
        ]);
    }


      /**
     * @Route("/register", name="register")
     */
    public function Register( Request $request , ObjectManager $manager, UserPasswordEncoderInterface $encoder,GenderRepository $repo){

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        $genders = $repo->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
            //  assigner le role par defaut au user qui s'inscrit
            $user->setRoles(['ROLE_USER']);
            // Hasher le password de user qui s'inscrit
            $hashedPassword = $encoder->encodePassword($user, $user->getPassword());
            // assigner le password hashé au user
            $user->setPassword($hashedPassword);
            $manager->persist($user);


            $manager->flush();
            $token = new UsernamePasswordToken(
                $user,
                $hashedPassword,
                'main',
                $user->getRoles()
            );

            $this->get('security.token_storage')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));

            $this->addFlash('success', 'You are now successfully registered!');

            return $this->redirectToRoute('index');

        }
        return $this->render('auth/register.html.twig',[
            "register" => $form->createView(),
            'genders' => $genders

        ]);
    }
}


