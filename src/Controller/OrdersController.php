<?php

namespace App\Controller;

use App\Entity\Orders;
use App\Entity\User;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\LineItemController;

class OrdersController extends AbstractController
{
    /**
     * @Route("/user/orders", name="user_orders")
     */
    public function index()
    {
        return $this->render('account/orders.html.twig');
    }

    /**
     * @Route("/user/order/confirm", name="confirm_order")
     */
    public function confirmOrder(ObjectManager $om)
    {

        $user = $this->getUser();
        $lineItems = $user->getLineItems();

        $order = new Orders();
        $order->setUser($user)
            ->setOrderDate(new DateTime())
            ->setStatus('en cours de validation');

        $total = null;

        foreach ($lineItems as $lineItem) {
            $order->addLineItem($lineItem);
            $total += $lineItem->getPrice();
        }

        $order->setTotal($total);

        $om->persist($order);

        foreach ($lineItems as $lineItem) {
            $om->remove($lineItem);
        }

        $om->flush();

        return $this->redirectToRoute('user_orders');
    }


    public function getUserLineItems(string $view) {
        $user = $this->getUser();
        $lineItems = $user->getLineItems();

        $total = null;

        foreach ($lineItems as $lineItem) {

            $total += $lineItem->getPrice();
        }

        return $this->render($view, [
            'total' => $total
        ]);
    }

    /**
     * @Route("/user/shopping_cart/checkout/pay", name="cart_pay")
     */
    public function pay() {
        return $this->getUserLineItems('shopping_cart/pay.html.twig');
    }

    /**
     * @Route("/user/shopping_cart/checkout", name="address_validate")
     */
    public function addressValidator(){
        return $this->getUserLineItems('shopping_cart/validate-address.html.twig');
    }
}
